import React, { Component } from "react"
import PropTypes from "prop-types"
import AcfComponent from '../components/acfComponent';
import Header from '../components/header';
import Footer from '../components/footer';

class PageTemplate extends Component {

    constructor(props) {

        super(props);

    }     

    generateComponentKey(pre) {

        return `${pre}_${Math.floor(Math.random() * 100000)}`;

    }

    render() {

        const currentPage = this.props.data.wordpressPage
        const siteMeta = this.props.data.site.siteMetadata
        const modules = this.props.data.wordpressPage.acf.modules_page

        console.log(modules);

        return (

            <div>

                <Header {...siteMeta}/>

                <main>

                    {modules.map((component) => {
                        return <AcfComponent key={this.generateComponentKey(component.__typename)} {...component} />
                    })}

                </main>

                <Footer {...siteMeta}/>

            </div>

        )

    }

}

export default PageTemplate

export const pageQuery = graphql`

query currentPageQuery($id: String!) {
    wordpressPage(id: { eq: $id }) {
        title
        content
        acf {
          modules_page {
            __typename
            ... on WordPressAcf_banner {
              banner_item { 
                banner_item_title
              }
            }
            __typename
            ... on WordPressAcf_textbox {
              textbox_content
              wrapper
            }
          }
        }
        date(formatString: "MMMM DD, YYYY")
    }
    site {
        id
        siteMetadata {
            title
            subtitle
        }
    }
}

`