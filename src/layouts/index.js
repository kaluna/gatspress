import React from "react"
import PropTypes from "prop-types"
import Link from "gatsby-link"

class DefaultLayout extends React.Component {

    constructor(props) {

        super(props);

    }

    componentDidMount() {

    }

  render() {
    return (
      <div>
       
       {this.props.children()}

      </div>
    )
  }
}

DefaultLayout.propTypes = {

    location: PropTypes.object.isRequired,

}

export default DefaultLayout