import React from 'react'
import Link from 'gatsby-link'
import Helmet from "react-helmet"

class Footer extends React.Component {

    constructor(props) {

        super(props);

    }

    componentDidMount() {

    }

    render() {

        let title = this.props.title;
        let subtitle = this.props.subtitle;

        return (

            <div>

                <footer>
                    
                    <div className="title"><i>{title}</i></div>

                    <div className="subtitle">{subtitle}</div>

                </footer>

            </div>

        )

    }

}

export default Footer
