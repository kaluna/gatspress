import React, { Component } from 'react';

class Banner extends Component {

    render() {
        
        const { banner_item: items = [] } = this.props;
        const { banner_item_title: title } = items[0];

        return (
            <h1>{title}</h1>
        )
        
    }

}

export default Banner;