import  React, { Component } from "react";
import { acfToComponent } from '../utils/acf-component-map'

class AcfComponent extends Component {

    render() {

        const { __typename, ...rest } = this.props;

        const OurComponent = acfToComponent(__typename);

        if(OurComponent) {
            return React.cloneElement(<OurComponent />, {
                ...rest
            })
        }
       
        return null;
    }

}

export default AcfComponent;