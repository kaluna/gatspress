import React, { Component } from 'react';

import Parser from 'html-react-parser';

class TextBox extends Component {

    render() {
        const { textbox_content, wrapper } = this.props;
        return (
            <div className={wrapper}>{Parser(textbox_content)}</div>
        )
        
    }

}

export default TextBox;