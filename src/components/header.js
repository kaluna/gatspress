import React from 'react'
import Link from 'gatsby-link'
import Helmet from "react-helmet"

class Header extends React.Component {

    constructor(props) {

        super(props);

    }

    componentDidMount() {

    }

    render() {

        let title = this.props.title;
        let subtitle = this.props.subtitle;

        return (

            <div>

                <Helmet
                    title={title}
                    meta={[
                        { name: 'description', content: subtitle }
                    ]}
                />

                <header>
                    
                    <div className="title"><i>{title}</i></div>

                    <div className="subtitle">{subtitle}</div>

                </header>

            </div>

        )

    }

}

export default Header
