import Banner from '../../components/banner';
import TextBox from '../../components/text-box';

const acfComponentMap = [
    {
        'acf': 'WordPressAcf_banner',
        'component': Banner
    },
    {
        'acf': 'WordPressAcf_textbox',
        'component': TextBox
    }
];

export const acfToComponent = (type) => {
    const mappedComponent = acfComponentMap.find(map => {
        return map.acf === type;
    });
    return mappedComponent ? mappedComponent.component : false;
};

export default acfToComponent;